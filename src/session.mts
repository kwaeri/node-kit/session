/**
 * SPDX-PackageName: kwaeri/session
 * SPDX-PackageVersion: 0.3.4
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as http from 'http';
import {
    ClientSession,
    SessionBits,
    SessionUserBits
} from './client-session.mjs';
import { BaseSessionStore } from '@kwaeri/session-store';
import { kdt } from '@kwaeri/developer-tools';
import debug from 'debug';


// DEFINES
const _     = new kdt(),
      DEBUG = debug( 'nodekit:session' );


export namespace NodeKit {
    export namespace http {
        export type RequestBits = {
            session: any;
        }

        export type ResponseBits = {
            session: any;
            setSession: any;
            redirect: any;
        }
    }
}


export class Session {
    private configuration;

    private ts?: any = null;

    private lut;

    public defaultCookiePath;

    public promoter;

    public store: any;


    /**
     * Class constructor
     *
     * @param { any } configuration
     *
     * @returns { Session } An instance of a {@link Session} object
     */
    constructor( configuration: any ) {
        this.configuration = configuration;
        this.defaultCookiePath = '/';

        if( this.configuration.appType === 'admin' )
            this.defaultCookiePath = '/admin';

        DEBUG( `Set 'defaultCookiePath' to '${this.defaultCookiePath}'` );

        this.promoter = this.configuration.promoter;
        //delete this.config.promoter;

        // Configure the store:
        this.configuration.session = _.set( this.configuration.session, "environment", this.configuration.environment );
        if( this.configuration.store && this.configuration.store !== "" )
            this.store = new this.configuration.store( _.get( this.configuration, 'session', {} ) );
        else
            this.store = new BaseSessionStore( _.get( this.configuration, 'session', {} ) );

        // Prepare a lookup table for our genId method. This saves JIT computation time
        // when we're generating random id's.
        this.lut = [];
        for( var i=0; i<256; i++ ) {
            this.lut[i] = ( i < 16 ? '0' : '' )+( i ).toString( 16 );
        };
    }


    /**
     * Searches for a session
     *
     * @param { http.IncomingMessage } request
     * @param { http.ServerResponse<http.IncomingMessage> } response
     * @param { any } callback
     *
     * @returns { [http.IncomingMessage, http.ServerResponse<http.IncomingMessage>] }
     */
    async find( request: http.IncomingMessage&NodeKit.http.RequestBits, response: http.ServerResponse<http.IncomingMessage>&NodeKit.http.ResponseBits, callback: any ): Promise<void|any[]> {
        let cookies,
            cfg: SessionBits = {},
            ts = new Date(),    // Control
            stamp: Date|number = new Date(), // We need two Date objects since the first will act as a control - its required to instantiate 2 of them.
            cstamp,
            expired = false;

        // Start by parsing the cookies
        cookies = this.getCookies( request );
        cookies.id = cookies.id || await this.getUniqueId();
        cookies.uk = cookies.uk || this.genId();

        DEBUG( `Get 'session' with 'id' [${cookies.id}]` );

        try {
            // Now check for the session by Id from the session store, and make sure the uk matches in order to further help in avoiding session take-over
            //if( await this.store.getSession( cookies.id ) && await this.store.get( cookies.id, 'uk' ) === cookies.uk /*this.store.hasOwnProperty( cookies.id ) && this.store[cookies.id].uk === cookies.uk*/ )
            const deferredCheck = await this.store.getSession( cookies.id );
            if( deferredCheck && deferredCheck.uk === cookies.uk ) {
                DEBUG( `Session check successful. uk: '${deferredCheck.uk}' cookies.uk '${cookies.uk}'` );

                // Grab the current cookie expiration from the session store and then update the value to the new stamp.
                cstamp = new Date( await this.store.get( cookies.id, 'expires' ) /*this.store[cookies.id].expires*/ );
                stamp.setTime( stamp.getTime() + ( 15 * 60 * 1000 ) );

                // If the session store's time stamp had expired, it just means the session cleanup cycle hasn't run, let's make sure to set
                // the authenticated value to false so that it is updated on the client's side.
                if( ts > cstamp )
                    await this.store.set( cookies.id, 'authenticated', false );

                //this.store[cookies.id].expires = stamp;
                await this.store.set( cookies.id, 'expires', stamp.getTime() );
            }
            else {   // Otherwise create a new session using the cookie provided Id or new session Id that was generated.
                cfg.id = cookies.id;
                cfg.uk = cookies.uk;
                cfg.host = this.configuration.url;
                cfg.path = this.defaultCookiePath;
                cfg.expires = stamp.getTime();
                cfg.authenticated = false;
                cfg.user = { username: 'Guest', email: 'you@example.com', name: { first: 'Guest' } };
                if( this.configuration.domain ) { cfg.domain = this.configuration.domain; }
                //this.store[cookies.id] = new ClientSession( cfg );
                if( !await this.store.createSession( cookies.id, new ClientSession( cfg ) ) )
                    return Promise.reject( new Error( `There was an issue creating the new session '${cookies.id}'. ` ) );

                DEBUG( `New 'session' with [id] '${cookies.id}' created` );
            }

            // We pass a copy of the client in the request, as well as the accepted cookies, and a method to fetch them
            request.session = {};
            request.session.client = await this.store.getSession( cookies.id ); /*this.store[cookies.id];*/
            request.session.cookies = cookies;
            request.session.get = Session.prototype.get;
            request.session.setUserAuth = this.moderate.bind( this );
            Object.defineProperty(
                request,
                'isAuthenticated',
                {
                    value: this.isAuthenticated( cookies.id! ),
                    writable: false,
                    enumerable: true,
                    configurable: false
                }
            );

            DEBUG( `Set 'props' in 'request' (sid,cookies,Function)` );

            // We assign an empty object to a property named cookies for allowing
            // cookies to be set, and assign a method to do just that. We also do
            // the same for sessionalso
            // construct and send the headers
            response.session = {};
            response.session.tools = this.configuration.rtools;   // Add these tools for the set function;
            response.session.id = cookies.id;
            response.session.uk = cookies.uk;
            response.session.url = await this.store.get( cookies.id, 'host' ); /* this.store[cookies.id].host;*/
            if( this.configuration.domain ) { response.session.domain = await this.store.get( cookies.id, 'domain' ); /*this.store[cookies.id].domain;*/ }
            response.session.defaultCookiePath = this.defaultCookiePath;
            response.session.cookies = [];
            response.session.genExpirationDate = Session.prototype.genExpirationDate;
            response.session.set = Session.prototype.set;
            response.session.set( 'sid', JSON.stringify( { i: cookies.id, d: cookies.uk } ), { secure: true } );
            response.setSession = Session.prototype.setSession;
            response.redirect = Session.prototype.redirect;

            DEBUG( `Set 'props' in 'response' (cookies,Function,headers)` );
        }
        catch( error ) {
            DEBUG( `[SESSION:FIND]: ERROR: '${error}'` );
            console.error( `[SESSION:FIND]: ERROR: '${error}'` );
        }

        //return Promise.resolve( callback( request, response ) )
        if( callback )
            callback( request, response );
        else
            return Promise.resolve( [ request, response ] );
    };


    /**
     * Searches for a session and returns its cookies
     *
     * @param { http.IncomingMessage&NodeKit.http.RequestBits } request
     *
     * @returns { SessionBits } A cookie object
     */
    getCookies( request: http.IncomingMessage&NodeKit.http.RequestBits ): SessionBits {
        // We start by splitting the string by ';'...the last one doesn't need to have one :)
        let cstring = null,
            cookies: SessionBits = {};

        DEBUG( `Get 'cookies'` );

        if( request.headers.cookie ) {
            cstring = request.headers.cookie.toString();
            cstring = cstring.split( ';' );

            // We have an array of strings, each with an =
            for( let key in cstring ) {
                let i = cstring[key].split( '=' );
                if( i ) {
                    // Remove any leading spaces from the keys
                    i[0] = i[0].replace( ' ', '' );

                    // Now we have a single cookie key and value
                    if( i[0] == 'sid' ) {
                        let ids = JSON.parse( i[1] );
                        cookies.id = ids.i;
                        cookies.uk = ids.d;
                    }
                    else {
                        ( cookies as any )[i[0]] = i[1];
                    }
                }
            }
        }

        return cookies;
    };


    /**
     * Generates a GUID.
     *
     * Based on the work of [Jeff Ward](http://stackoverflow.com/a/21963136), for the performance, and the saved time!
     *
     * @param { void }
     *
     * @returns { string }
     */
    genId(): string {
        const d0 = Math.random()*0xffffffff|0;
        const d1 = Math.random()*0xffffffff|0;
        const d2 = Math.random()*0xffffffff|0;
        const d3 = Math.random()*0xffffffff|0;

        return  this.lut[d0&0xff]+this.lut[d0>>8&0xff]+this.lut[d0>>16&0xff]+this.lut[d0>>24&0xff]+'-'+
                this.lut[d1&0xff]+this.lut[d1>>8&0xff]+'-'+this.lut[d1>>16&0x0f|0x40]+this.lut[d1>>24&0xff]+'-'+
                this.lut[d2&0x3f|0x80]+this.lut[d2>>8&0xff]+'-'+this.lut[d2>>16&0xff]+this.lut[d2>>24&0xff]+
                this.lut[d3&0xff]+this.lut[d3>>8&0xff]+this.lut[d3>>16&0xff]+this.lut[d3>>24&0xff];
    };


    /**
     * Gets a unique ID for a new client session
     *
     * @param { void }
     *
     * @returns { Promise<any> } The promise for a unique id
     */
    async getUniqueId(): Promise<any> {
        const id                = this.genId(),
              deferredPotential = await this.store.getSession( id );

        if( deferredPotential )
            return Promise.resolve( await this.getUniqueId() );

        return Promise.resolve( id );
    }


    /**
     * Moderates the session from the controller
     *
     * @param { string } sid
     * @param { any } user
     * @param { boolean } unauth
     *
     * @returns { Promise<void> } The promise of void
     */
    async moderate( sid: any, user: any, unauth: boolean ): Promise<void> {
        if( !unauth ) {
            await this.store.set( sid, 'authenticated', true );
            await this.store.set( sid, 'user', user );

            DEBUG( `${sid} is now authenticated: ${await this.store.get( sid, "authenticated" )}` );
        }
        else {
            await this.store.deleteSession( sid );

            DEBUG( `${sid} is now signed out.` );
        }
    };


    /**
     * Checks if client is authenticated
     *
     * @param { string } sid
     *
     * @returns { Promise<boolean> } The promise of a boolean
     */
    async isAuthenticated( sid: string ): Promise<boolean> {
        DEBUG( `Get 'authenticated' for '${sid}'` );

        return await this.store.get( sid, 'authenticated' ); /*this.store[sid].authenticated;*/
    };


    /**
     * Generates a cookie expiration date string
     *
     * @param { number } minutes of days the cookie should persist
     *
     * @returns { string }
     */
    genExpirationDate( minutes: number ): string {
        const d = new Date();
        d.setTime( d.getTime() + ( minutes * 60 * 1000 ) );

        DEBUG( `Set 'expires' to '%s'`, d.toUTCString() );

        return d.toUTCString();
    };


    /**
     * Starts the session service. The session runs cleanup ever 5 minutes (default)
     *
     * @param { number } minutes The number of minutes that should span between session cleanup
     *
     * @return { void }
     */
    async start( minutes: number = 5 ): Promise<void> {
        // For now we will default the timer to every 5 minutes.
        this.ts = new Date();

        await this.cleanup( ( minutes*60*1000 ) );
    };


    /**
     * Sets a Data/session variable
     *
     * @param { string } name
     * @param { any } value
     * @param { any } options
     *
     * @return { boolean }
     */
    set( name: string, value: any, options: any ): boolean {
        let def = {
            host: ( this as any ).url,
            domain: ( this as any ).domain,
            path: this.defaultCookiePath,
            secure: false,
            expires: this.genExpirationDate( 30 )
        },
        o = _.extend( options || {}, def );

        if( ( o as any ).secure )
            ( o as any ).secure = 'HttpOnly';

        if( name && value ) {
            if( name === 'user' )
                value = JSON.stringify( value );

            ( this as any ).cookies.push(`${name}=${value}; ${((( o as any ).domain)?`domain=${( o as any ).domain}; `:`host=${( o as any ).host}; `)}path=${( o as any ).path}; expires=${( o as any ).expires}; ${( o as any ).secure}` );

            DEBUG(`Set 'cookie' [${name}] to '${value} with ${((( o as any ).domain)?`domain=${( o as any ).domain}; `:`host=${( o as any ).host}; `)} path='${( o as any ).path}'; expires='${( o as any ).expires}'; ${( o as any ).secure}`);

            return true;
        }

        DEBUG( `Call 'set' [cookie] with '${name}'` );

        return false;
    };

    /**
     * Gets a Data/session variable
     *
     * @param { string } name
     * @param { any } def
     *
     * @returns { any }
     */
    get( name: string, def: any ): any {
        if( name ) {
            DEBUG( `Call 'get' [cookie] with '${name}'` );

            if( ( this as any ).cookies.hasOwnProperty( name ) ) {
                // Add logic for supporting proprietary user cookie
                if( name === 'user' )
                    return JSON.parse( ( this as any ).cookies[name] );
                else
                    return ( this as any ).cookies[name];
            }
            else {
                if( def )
                    return def;
                else // Let's set the default user cookie if it was requested, but not set, and defaults not provided.
                    if( name === 'user' )
                        return { username: 'Guest', email: 'you@example.com', name: { first: 'Guest' } };
                    else
                        return {};
            }
        }

    };


    /**
     * Sets the sessions cookies
     *
     * @returns { void }
     */
    setSession(): void {
        // setHeader is a member of the response object:
        ( this as any ).setHeader( 'Set-Cookie', ( this as any ).session.cookies );
    };


    /**
     * Sends a redirect header to the client
     *
     * @param { string } path
     *
     * @returns { void }
     */
    redirect( path: string ): void {
        DEBUG( `Execute client redirect` );

        // Set the response status:
        ( this as unknown as http.ServerResponse ).statusCode = 302;

        DEBUG( `Set 'status' [HTTP] to '302'` );

        // Set the cookie headers
        this.setSession();

        // Set the location header:
        ( this as unknown as http.ServerResponse ).setHeader( 'Location', path );

        DEBUG( `Set 'location' to '${path}'` );

        // Write the
        ( this as unknown as http.ServerResponse ).end( '<p>Please wait while we redirect you.</p>' );
    };


    /**
     * Sends a redirect header to the client
     *
     * @param { string } path
     *
     * @returns { void }voiday
     *
     * @returns { Promise<void> }
     */
    async cleanup( delay: number ): Promise<void> {
        const reaper = this,
            count = await this.store.countSessions(),
            ts = new Date();

        DEBUG( `Session clean up started` );

        this.store.cleanSessions();

        /*
            Calculating difference in time:  Based strongly on:
            https://stackoverflow.com/a/13904120
        */

        // Get total seconds between the times:
        let delta = Math.abs( ts.getTime() - this.ts.getTime() ) / 1000;

        // Calculate (and subtract) whole days
        const days = Math.floor( delta / 86400 );
        delta -= days * 86400;

        // Calculate (and subtract) whole hours
        const hours = Math.floor( delta / 3600 ) % 24;
        delta -= hours * 3600;

        // Calculate (and subtract) whole minutes
        const minutes = Math.floor( delta / 60 ) % 60;
        delta -= minutes * 60;

        // What's left is seconds, which we round up to the nearest whole number
        const seconds = Math.round( delta % 60 );

        // Round those seconds up to the nearest whole number!
        //seconds = Math.round( seconds );

        console.log( `${count} active client sessions. Up since ${this.ts} (${(( days ) ? `${days} days ` : '' )}${(( hours ) ? `${hours} hours ` : ``)}${(( minutes ) ? `${minutes} minutes ` : ``)}${(( seconds ) ? `${seconds} seconds ` : ``)})` );

        setTimeout( function(){ reaper.cleanup( delay ); }, delay );
    };
}

